# Poker hands

Run instructions:

```
docker-compose up
```

This will run nginx proxying to a dotnet core backend and hosting a static
frontend.

Completed:

* backend api takes player hands and returns the player by order of who won, along with their reason for winning:
```bash
$ curl -H "Content-Type: application/json" -X POST http://localhost:9000/api/poker -d '{"hands":[{"player":"joe","cards":["6C","6H","6D","5C","9C"]},{"player":"jan","cards":["6C","6H","6D","5C","10D"]}]}'
[{"player":"jan","score":{"rule":"ThreeOfAKind","cardSequence":[4,8,3]}},{"player":"joe","score":{"rule":"ThreeOfAKind","cardSequence":[4,7,3]}}]
```
* frontend overall look-and-feel

Missing pieces:

* backend and frontend not completely wired together
* missing unit tests
