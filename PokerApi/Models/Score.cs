using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PokerApi.Models
{
    public enum RuleValues {
        HighCard,
        Pair,
        TwoPairs,
        ThreeOfAKind,
        Straight,
        Flush,
        FullHouse,
        FourOfAKind,
        StraightFlush
    }

    public class Score {
        [JsonConverter(typeof(StringEnumConverter))]
        public RuleValues Rule {get; set; }
        public IEnumerable<int> CardSequence { get; set; }

        public static Score ScoreCards(IEnumerable<Card> cards){
            if (IsFlush(cards) && IsStraight(cards)){
                return new Score {
                    Rule = RuleValues.StraightFlush,
                    CardSequence = cards.Select(item=> item.Rank).Take(1)
                };
            }
            var groupedCards = cards.GroupBy(item => item.Rank)
                .OrderByDescending(item => Tuple.Create(item.Count(), item.First().Rank));
            if (groupedCards.Take(1).All(x => x.Count() == 4)){
                return new Score {
                    Rule = RuleValues.FourOfAKind,
                    CardSequence = groupedCards.First().Select(x => x.Rank).Take(1)
                };
            }
            if (groupedCards.Any(x => x.Count() == 3) && groupedCards.Any(x => x.Count() == 2)){
                return new Score {
                    Rule = RuleValues.FullHouse,
                    CardSequence = groupedCards.Select(items => items.First().Rank)
                };
            }
            if (IsFlush(cards)) {
                return new Score {
                    Rule = RuleValues.Flush,
                    CardSequence = cards.Select(item => item.Rank).OrderByDescending(x => x)
                };
            }
            if (IsStraight(cards)){
                return new Score {
                    Rule = RuleValues.Straight,
                    CardSequence = cards.Select(item => item.Rank).Take(1)
                };
            }
            if (groupedCards.Any(x => x.Count() == 3)){
                return new Score {
                    Rule = RuleValues.ThreeOfAKind,
                    CardSequence = groupedCards.Select(items => items.First().Rank)
                };
            }
            if (groupedCards.Where(x => x.Count() == 2).Count() == 2){
                return new Score {
                    Rule = RuleValues.TwoPairs,
                    CardSequence = groupedCards.Select(items => items.First().Rank)
                };
            }
            if (groupedCards.Any(x => x.Count() == 2)){
                return new Score {
                    Rule = RuleValues.Pair,
                    CardSequence = groupedCards.Select(items => items.First().Rank)
                };
            }
            return new Score {
                Rule = RuleValues.HighCard,
                CardSequence = cards.Select(item => item.Rank).OrderByDescending(x => x)
            };
        }

        public static bool IsFlush(IEnumerable<Card> cards){
            return cards.GroupBy(item => item.Suit).Count() == 1;
        }

        public static bool IsStraight(IEnumerable<Card> cards){
            Card prevCard = null;
            foreach (Card card in cards.OrderByDescending(item => item.Rank)){
                if(prevCard != null && prevCard.Rank - 1 != card.Rank){
                    return false;
                }
                prevCard = card;
            }
            return true;
        }
    }

    public class ComparableSequence : IComparable {
        public IEnumerable<int> Items { get; set; }
        
        public int CompareTo(object obj){
            var other = (ComparableSequence)obj;
            var mismatches = Items.Zip(other.Items, (a, b) => a.CompareTo(b)).Where(item => item != 0);
            if (!mismatches.Any()){
                return 0;
            }
            return mismatches.First();
        }
    }
}
