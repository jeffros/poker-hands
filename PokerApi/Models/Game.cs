using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace PokerApi.Models
{
    public class Game : IValidatableObject
    {
        [Required]
        public List<Hand> Hands { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext) {
            Game game = (Game)validationContext.ObjectInstance;
            if (game.Hands.Count < 1){
                yield return new ValidationResult("At least one hand is required.");
            }
        }
    }

    public class Hand : IValidatableObject
    {
        [Required]
        public string Player { get; set ; }

        [Required]
        public IEnumerable<string> Cards { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext) {
            Hand hand = (Hand)validationContext.ObjectInstance;
            if (hand.Cards.Count() != 5){
                yield return new ValidationResult("Five Cards are required");
            }
            var invalidCards = hand.Cards.Where(item => !Card.IsValid(item));
            foreach (string invalidCard in invalidCards){
                yield return new ValidationResult("Invalid card value " + invalidCard);
            }
        }

        public PlayerScore ScoreHand(){
            return new PlayerScore {
                Player = Player,
                Score = Score.ScoreCards(Cards.Select(item => Card.Parse(item)))
            };
        }
    }

    public class Card
    {
        static char[] suitValues = new char[] {'H', 'C', 'S', 'D'};
        static string[] rankValues = new string[] {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};

        public int Suit { get; set;}
        public int Rank { get; set; }

        public static Card Parse(string value) {
            if (value == null || value.Length < 2){
                throw new System.ArgumentException("Bad Card value " + value);
            }
            int possibleSuit = Array.IndexOf(suitValues, value[value.Length - 1]);
            int possibleRank = Array.IndexOf(rankValues, value.Substring(0, value.Length - 1));
            if (possibleRank == -1 || possibleSuit == -1) {
                throw new System.ArgumentException("Bad Card value " + value);
            }
            return new Card {Suit = possibleSuit, Rank = possibleRank};
        }

        public static bool IsValid(string value) {
            try {
                Card.Parse(value);
            }
            catch (Exception) {
                return false;
            }
            return true;
        }
    }

    public class PlayerScore {
        public string Player {get; set;}
        public Score Score { get; set; }
    }
}