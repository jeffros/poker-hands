﻿#define Primary
#if Primary
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PokerApi.Models;

#region PokerController
namespace PokerApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PokerController : Controller
    {
        [HttpPost]
        public JsonResult RankWinners(Game game){
            var scores = game.Hands
                .Select(item => item.ScoreHand())
                .OrderByDescending(item => item.Score.Rule)
                .ThenByDescending(item => new ComparableSequence{Items = item.Score.CardSequence});
            return Json(scores);
        }
    }
}
#endregion
#endif